import cmd
import sys

def parse_arg(arg):
	return arg.split()

class ExtShell(cmd.Cmd):
	version = '1.0'
	intro = '[COMMAND SHELL]\n[v ' + version + ']'
	prompt = '> '
	file = None

	# EXIT
	def help_exit(self):
		print("Exit the program.\n")
		print("Syntax :")
		print("    exit [/?]")
		return
	def do_exit(self, arg):
		lArg = parse_arg(arg)
		if len(lArg) >= 1:
			for (k, v) in enumerate(lArg):
				if v == '/?':
					self.help_exit()
					return
			return True
		else:
			return True

	# ECHO
	def help_echo(self):
		print("Print a text to the screen.\n")
		print("Syntax :")
		print("    echo <string>|/?")
	def do_echo(self, arg):
		lArg = parse_arg(arg)
		if len(lArg) >= 1:
			if lArg[0] == "/?":
				self.help_echo()
				return
			else:
				print(arg)
				return
			return
		return
	
	# precmd command
	# to do some stuff before the command is executed
	def precmd(self, line):
		lineFirstCharacter = line[:1]
		lineWithoutFirstCharacter = line[1:]
		
		if lineFirstCharacter == "&":
			# execute a script
			with open(lineWithoutFirstCharacter) as i:
				self.cmdqueue.extend(f.read().splitlines())
		
		return line
	

ExtShell().cmdloop()
